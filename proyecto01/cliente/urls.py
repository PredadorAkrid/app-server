from django.urls import path, re_path
from django.conf.urls import url, include
from .views import *
from cliente import views
from .models import *
app_name = "cliente"


# Url's para cliente
urlpatterns = [
    # Vistas basadas en clases y funciones
    path("", views.Index.as_view(), name="IndexCliente"),
    path("registro-cliente/", views.RegistroCliente.as_view(), name="registro_cliente"),
    path("home/", HomeCliente, name="HomeCliente"),
    path("cerrar-sesion/", views.CerrarSesion.as_view(), name="cerrar_sesion"),
]